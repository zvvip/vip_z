(function () {
    // 渲染商家
    // location.href = `../pages/gsproduct.html?shopid=0&&areaid=0`
    $.ajax({
            url: `http://chst.vip:1234/api/getgsshop`
        })
        .then(res => {
            console.log(res);
            let newData = [...res.result]
            console.log(newData);
            let page = `
                <div class="clkk">
                    <span>${newData[0].shopName}</span>
                </div>
        `
            $('#cityNav').html(page)

            let html = ''
            newData.forEach(item => {
                html += `
            <li shopid="${item.shopId}" shopname="${item.shopName}">${item.shopName}</li>
            `
            })
            $('#cityNav').on('click', '.clkk', function () {
                $('.inforB').hide()
                $('.inforA').html(html)
                $('.inforA').slideToggle()
            })
        })
    // 渲染地区
    $.ajax({
            url: 'http://chst.vip:1234/api/getgsshoparea'
        })
        .then(res => {
            let newData = [...res.result]

            let areaName = newData[0].areaName.substr(0, 2);
            let page = `
    <div class="clkkk">
        <span>${areaName}</span>
    </div>
    <div>免费区</div>
    <div>
        <span></span>
    </div>
    `
            $('#cityNav').append(page)
            let html = ''
            newData.forEach(item => {
                html += `
            <li areaid="${item.areaId}" areaname="${item.areaName}">${item.areaName}</li>
            `
            })
            $('#cityNav').on('click', '.clkkk', function () {
                $('.inforA').hide()
                $('.inforB').html(html)
                $('.inforB').slideToggle()
                // console.log($('this').attr('areaid'));
            })
        })
    // 点击获取商品列表
    $('.inforA').on('click', 'li', function () {
        let shopid = $(this).attr('shopid');
        let shopname = $(this).attr('shopname')
        // console.log(shopname);
        $('.clkk span').html(shopname)
        // let qr = getUrlParams(location.href)
        // console.log(qr);

        // location.href = `../pages/gsproduct.html?shopid=${shopid}&areaid=${qr.areaid}`
        $.ajax({
                url: `http://chst.vip:1234/api/getgsproduct?shopid=${shopid}&areaid=0`
            })
            .then(res => {
                let lisArr = [...res.result]
                let prolis = ''
                lisArr.forEach(item => {
                    prolis += `
                        <li>
                            <a href="">
                                <img src="${item.productImg}" alt="">
                                <div class="title">${item.productName}</div>
                                <p class="price">${item.productPrice}</p>
                            </a>
                        </li>
                `

                })
                $('#proList').html(prolis)
            })
            $('.inforA').hide()
    })
    $('.inforB').on('click', 'li', function () {
        let areaid = $(this).attr('areaid');
        let areaname = $(this).attr('areaname').substr(0, 2)
        // console.log(areaname);
        $('.clkkk span').html(areaname)
        // let qr = getUrlParams(location.href)
        // location.href = `../pages/gsproduct.html?shopid=${qr.shopid}&areaid=${areaid}`
        $.ajax({
                url: `http://chst.vip:1234/api/getgsproduct?shopid=0&areaid=${areaid}`
            })
            .then(res => {
                let lisArr = [...res.result]
                let prolis = ''
                lisArr.forEach(item => {
                    prolis += `
                            <li>
                                <a href="">
                                    <img src="${item.productImg}" alt="">
                                    <div class="title">${item.productName}</div>
                                    <p class="price">${item.productPrice}</p>
                                </a>
                            </li>
                    `

                })
                $('#proList').html(prolis)
            })
            $('.inforB').hide()
    })


    // 进入页面默认渲染
    fetch(`http://chst.vip:1234/api/getgsproduct?shopid=0&areaid=0`)
        .then(body => body.json())
        .then(res => {
            let lisArr = [...res.result]
            console.log(lisArr);
            let prolis = ''
            lisArr.forEach(item => {
                prolis += `
                        <li>
                            <a href="">
                                <img src="${item.productImg}" alt="">
                                <div class="title">${item.productName}</div>
                                <p class="price">${item.productPrice}</p>
                            </a>
                        </li>
                `

            })
            document.querySelector('#proList').innerHTML = prolis
        })
})()