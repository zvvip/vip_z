class Person {
    constructor() {
        this.requestPro()
        this.infoList = document.querySelector('#infoList')
    }

    // 请求
    requestPro() {
        fetch('http://chst.vip:1234/api/getinlanddiscount')
            .then(body => body.json())
            .then(res => {
                this.render(res.result);
            
            })
    }
    // 点击进入详情页
   
    render(data = []) {
        let newData = [...data]
        console.log(newData);
        let page = ''
        let count = 0;
        newData.forEach(item => {
            count++;
            if (count > 4) {
                return
            }
            // console.log(count);
            page += `
            <div class="inList" proid="${item.productId}">
            <a class="clearfix">
                <div class="img">
                    ${item.productImg}
                </div>
                <div class="textBot">
                    <h4>${item.productName}</h4>
                    <p>${item.productPrice}</p>
                    <p class="mall">${item.productFrom}|${item.productTime}</p>
                </div>
            </a>
        </div>
            `
        })
        // console.log(page);
        this.infoList.innerHTML = page
        $('#infoList').on('click','.inList',function(){
            let proid = $(this).attr('proid');
            console.log(proid);
            location.href = `../pages/discount.html?productid=${proid}`
        })
    }
}
let person = new Person()