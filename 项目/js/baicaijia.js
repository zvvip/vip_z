
//获取顶栏数据

fetch('http://chst.vip:1234/api/getbaicaijiatitle')
.then((body)=>body.json())
.then((res)=>{
    console.log(res);
    //清空数据
    let html = ""
    // console.log({result:data}=res);
    //遍历渲染页面
    res.result.forEach(item => {
        //模板字符串渲染
        html +=`
        <li class="${item.titleId}">
        ${item.title}
        </li>   `
    });
    document.querySelector('#nav ul').innerHTML =html
})

//事件监听
$('.ul_box').on('click',function(e){
    //监听属性点击的时候被点亮
    $(e.target).css('color',"red").siblings().css('color','black')
    console.log(e);
    console.log(e.target.className);
    //属性赋值
    let titleid = e.target.className
    // 获取列表
    fetch('http://chst.vip:1234/api/getbaicaijiaproduct?titleid='+titleid)
    .then((body)=>body.json())
    .then((res)=>{
        console.log(res);
        //数值清空
        let html1 = ""
        //遍历数据
        res.result.forEach((item)=>{
            // console.log(item);

            // 1.渲染图片
            // 2.渲染商品名称
            // 3.渲染商品价格
            // 4.渲染已领数量
            // 5.传染优惠卷
            // 6.渲染下单连接
            html1 +=`
            <li>
            <div class="img">${item.productImg}</div>  
            <div class="title"><h4>${item.productName}</h4></div>
            <div class="money">${item.productPrice}</div>
            ${item.productCouponRemain}
            ${item.productCoupon}
            <b>${item.productHref}</b>
            </li> `
        })
        document.querySelector('#navv ul').innerHTML = html1
    })

})

//获取分类列表

fetch('http://chst.vip:1234/api/getbaicaijiaproduct?titleid=0')
.then((body)=>body.json())
.then((res)=>{
      //数值清空
      let html1 = ""
      //遍历数据
      res.result.forEach((item)=>{
          // console.log(item);

          // 1.渲染图片
          // 2.渲染商品名称
          // 3.渲染商品价格
          // 4.渲染已领数量
          // 5.传染优惠卷
          // 6.渲染下单连接
          html1 +=`
          <li>
          <div class="img">${item.productImg}</div>  
          <div class="title"><h4>${item.productName}</h4></div>
          <div class="money">${item.productPrice}</div>
          ${item.productCouponRemain}
          ${item.productCoupon}
          <b>${item.productHref}</b>
          </li> `
      })
      document.querySelector('#navv ul').innerHTML = html1
})