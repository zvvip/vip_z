(function () {
    let qr = getUrlParams(location.href)
    let title = qr.title;
    $.ajax({ url: `http://chst.vip:1234/api/getbrandproductlist?brandtitleid=${qr.brandtitleid}` })
        .then(res => {
            console.log(res);
            let newData = [...res.result]
            // console.log(newData);
            let html = `<a>${title}&gt;</a>`
            $('.backup').append(html)
            let page = ''
            newData.forEach(item => {
                page += `
            <li categoryid="${item.categoryId}" productid="${item.productId}">
                <div class="img fl" >
                    ${item.productImg}
                </div>
                <div class="infoR">
                    <div class="title">
                        <h4>${item.productName}</h4>
                        <div class="price">${item.productPrice}</div>
                    </div>
                    <div class="other">
                        <span class="take_price">${item.productQuote}</span>
                        <i class="iconfont icon-xiaoxi talkbaout">${item.productCom}(2286)</i>
                    </div>
                </div>
            </li>
            `
            })
            $('.info').html(page)

            $('.info').on('click', 'li', function () {
                let categoryid = $(this).attr('categoryid');
                let productid = $(this).attr('productid');
                let proname = $(this).attr('proname');
                location.href = `../pages/productlist.html?categoryid=${categoryid}&productid=${productid}&title=${title}`
            })
        })
})()