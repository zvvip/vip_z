let proList = (function () {
    // let newData = [] //接受请求到的数据
    function render(data = []) {
        let newData = [...data]
        console.log(newData);
        let page = ''
        newData.forEach(item => {
            let regExp = /(?<=imgurl=)[^'"]*/;
            let r = item.productImg2.match(regExp);

            page += `
            <li proid = "${item.productId}">
            <img src=${r[0]} height="100" alt="">

           
                <div class="infoR">
                    <div class="title">
                        <h4>${item.productName}
                            <span>${item.productPinkage}</span>
                        </h4>
                    </div>
                    <div class="other">
                        <span class="mall fl">${item.productFrom}|${item.productTime}</span>
    
                        <em class="fr">
                            <i class="iconfont icon-xiaoxi "></i>
                            <span> ${item.productComCount}</span>
                        </em>
    
                    </div>
    
                </div>
            
        </li>
            `
        })
        $('.info').html(page)
    }
    // 点击事件
    $('.info').on('click','li',function(){
        let proid = $(this).attr('proid');
        location.href = `../pages/moneyproduct.html?productid=${proid}`
    })

    function toglePage(){
        if (countId === 0) {
            $('.prev').attr({
                'disabled': true,
                'style': 'color: rgb(204, 204, 204)'
            })
        } else {
            $('.prev').attr({
                'disabled': false
            })
        }
        if (countId === 13) {
            $('.next').attr({
                'disabled': true,
                'style': 'color: rgb(204, 204, 204)'
            })
        } else {
            $('.next').attr({
                'disabled': false
            })
        }
    }

    let countId = 0;
    // 点击下一页
    $('.next').click(function () {
        $('.prev').attr({
            'disabled': false,
            'style': 'color: rgb(51, 51, 51)'
        })
        countId++;
        toglePage()
        $.ajax({
                url: 'http://chst.vip:1234/api/getmoneyctrl',
                data: {
                    pageid: countId
                },
                catch: true
            })
            .then(res => {
                render(res.result) // 把获取到的数据当作参数传入
            })
            $('select.page').get(0).options[countId].selected = true
    })
    //点击上一页
    $('.prev').click(function () {
        $('.next').attr({
            'disabled': false,
            'style': 'color: rgb(51, 51, 51)'
        })
        countId--;
        toglePage()
        $.ajax({
                url: 'http://chst.vip:1234/api/getmoneyctrl',
                data: {
                    pageid: countId
                },
                catch: true
            })
            .then(res => {
                render(res.result) // 把获取到的数据当作参数传入
            })
            $('select.page').get(0).options[countId].selected = true
    })
    // 选中翻页
    $("select.page").change(function () {
        // console.log($(this).prop('selectedIndex'));
       countId = $(this).prop('selectedIndex')
       toglePage()

        $.ajax({
                url: 'http://chst.vip:1234/api/getmoneyctrl',
                data: {
                    pageid: countId
                },
                catch: true
            })
            .then(res => {
                render(res.result) // 把获取到的数据当作参数传入
            })



    });


    // ajax请求
    $.ajax({
            url: 'http://chst.vip:1234/api/getmoneyctrl',
            data: {
                pageid: 0
            },
            catch: true
        })
        .then(res => {
            render(res.result) // 把获取到的数据当作参数传入
        })
})()
