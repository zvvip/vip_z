function getUrlParams(url) {
    //1.对url进行处理,把?号后面的参数匹配出来,使用正则进行匹配
    //http://127.0.0.1:5500/detail.html?id=20&age=99
    let reg = /^[^\s]+\?([^&\s\d]+=[^\s]+)*/
    let match = reg.exec(url)
    match = match[1]
    if (!match) {//表示没匹配到
        console.warn("没有匹配到相应的query参数")
        return null
    }
    // console.log(match);
    //对参数进行uri解码
    let queryString = decodeURI(match);
    // console.log(queryString);
    //将queryString以"&"符号为分割点 切割成为一个数组
    let queryArr = queryString.split("&");
    // console.log(queryArr);
    let queryParamsObj = {}//用于存储处理好的query数据
    queryArr.forEach(item => {
        let itemArr = item.split("=") //对每个item 'id=20'这样的切割成['id','20']
        queryParamsObj[itemArr[0]] = itemArr[1]
    })
    // console.log(queryParamsObj);
    //将最终处理的结果返回
    return queryParamsObj
}   